FROM ruby:2.5
RUN apt-get update -qq && apt-get install -y nodejs postgresql-client
RUN mkdir /myapp
RUN mkdir ./var ; mkdir ./var/lib; mkdir ./var/lib/postgresql/; mkdir ./var/lib/postgresql/data
WORKDIR /myapp
COPY Gemfile /myapp/Gemfile
COPY Gemfile.lock /myapp/Gemfile.lock
RUN apt-get install imagemagick libmagickcore-dev libmagickwand-dev
RUN bundle install
COPY . /myapp

EXPOSE 3000

# Start the main process.
CMD ["rails", "server", "-b", "0.0.0.0","-e","production"]
