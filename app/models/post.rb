class Post < ActiveRecord::Base

    belongs_to :user
    #Pagination
    max_paginates_per 10
    #Photo uploader
    mount_uploader :image, PhotoUploader
end
