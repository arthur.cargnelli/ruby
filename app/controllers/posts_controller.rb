class PostsController < ApplicationController
    before_action :user_login
    before_action :load_user
    before_action :load_post, only: [ :show, :edit, :update, :creator, :destroy]
    before_action :creator, only: [ :edit, :update, :destroy]

    #Check if the user is signed in
    def user_login
        if !current_user
            redirect_to '/users/sign_in'
        end
    end
    #Check is the current user is the creator
    def creator
        @creator = User.find(@post.user_id)
        if @user != @creator
            redirect_to "/422"
        end
    end

    def new
        @post = Post.new
    end
    #load the current user
    def load_user
        @user = current_user
    end
    #load posts of the current user
    def load_post
        @post = Post.find(params[:id])
    end
    #Homepage 
    def index
        @posts = Post.page(params[:page]).per(10)
    end
    
    def create
        post_params = params.require(:post)
        post_params = post_params.permit(:title, :body, :image)
        @post = @user.posts.create(post_params)
        if @post.valid?
            redirect_to [@user,@post]
        else
            render:new
        end
    end

    def update
        post_params = params.require(:post)
        post_params = post_params.permit(:title, :body, :image)
        if @post.update(post_params)
            redirect_to [@user,@post]
        else
            render:edit
        end
    end

    def show
        @creator = User.find(@post.user_id)
    end 

    def destroy
        @post.delete
        redirect_to @user
    end  

end
