class UsersController < ApplicationController
    before_action:load_user, only: [ :show, :edit, :update, :delete]

    def show
        @posts = @user.posts #all the posts of a user
    end      
    
    def update
        user_params=get_params
        if @user.update(user_params)
            redirect_to @user
        else
            render:edit
        end
    end

    def get_params
        user_params = params.require(:user)
        user_params = user_params.permit(:first_name, :last_name, :birth_date) #allow parameters to be saved
    end
    def destroy
        Post.where(user_id:@user.id).delete_all #delete all post of a user
        User.where(id:@user).delete #delete user
        redirect_to "/"
    end  
    #load user from id
    def load_user
        @user = User.find(params[:id])
    end
end